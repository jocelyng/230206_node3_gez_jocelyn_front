import React, { useState } from 'react';
import axios from 'axios';
import {useNavigate } from 'react-router-dom'

const CustomerForm = () => {
  const [FirstName, setFirstName] = useState('');
  const [LastName, setLastName] = useState('');
  const [Adresse, setAdresse] = useState('');
  const [Tel, setTel] = useState('');
  const navigate = useNavigate()



  const handleSubmit = async (e) => {
    e.preventDefault();
      if (FirstName !== '' && LastName !== '' && Adresse !== '' && Tel !== '') {
        try {
          const res = await axios.post('http://localhost:8080/createOneCustomer', {
            "Firstname" : FirstName, "Lastname" : LastName, "Adresse" :Adresse, "Tel" :Tel
          });
    
          localStorage.setItem('token', JSON.stringify(res.data.token));
          localStorage.setItem('idCustomer', JSON.stringify(res.data.idCustomer));
          console.log(res.data.idCustomer)
          
          navigate('/Game')
        } catch (error) {
          console.error(error);
        }
      } else {
        alert('Tous les champs sont obligatoires');
      }
    };



  return (
    <form onSubmit={handleSubmit}>
      <label>
        First Name:
        <input type="text" value={FirstName} onChange={(e) => setFirstName(e.target.value)} />
      </label>
      <br />
      <label>
        Last Name:
        <input type="text" value={LastName} onChange={(e) => setLastName(e.target.value)} />
      </label>
      <br />
      <label>
        Adresse:
        <input type="text" value={Adresse} onChange={(e) => setAdresse(e.target.value)} />
      </label>
      <br />
      <label>
        Tel:
        <input type="text" value={Tel} onChange={(e) => setTel(e.target.value)} />
      </label>
      <br />
      <button type="submit">Submit</button>
    </form>
  );
};


export default CustomerForm;