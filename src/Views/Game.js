import axios from 'axios';
import React, { useState, useEffect } from 'react';
import {useNavigate } from 'react-router-dom'

const Game  = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [isTokenExpired, setIsTokenExpired] = useState(false);   
    const [dices, setDices] = useState([]);
    const [nbvaleur, setNbvaleur] = useState(0);
    const [nbessai, setNbessai] = useState(5);
    const [idCustomer, setIdCustomer] = useState(0);
    const [Pastry, setPastry] = useState([]);
    const [Customer, setCustomer] = useState([]);

    const navigate = useNavigate()


    //--------------Pastry------------------
    useEffect(() => {
        const fetchCustomer = async () => {
        const response = await fetch(`http://localhost:8080/getOneCustomer/${idCustomer}`);
        const Customer = await response.json();
        setCustomer(Customer);
        };
        fetchCustomer();
    }, [idCustomer]);
    //--------------------------------------

    //--------------Pastry------------------
    useEffect(() => {
        const fetchPastry = async () => {
        const response = await fetch('http://localhost:8080/getAllPastry');
        const Pastry = await response.json();
        setPastry(Pastry);
        };
        fetchPastry();
    }, []);
    //--------------------------------------

    //--------------Token------------------
    useEffect(() => {
        const intervalId = setInterval(() => {
        const token = localStorage.getItem("token");
        setIdCustomer(localStorage.getItem("idCustomer"));
        const tokenExpiration = JSON.parse(atob(token.split(".")[1])).exp;

        if (tokenExpiration * 1000 < Date.now()) {
            localStorage.removeItem("token");
            setIsTokenExpired(true);
        }
        }, 1000);

        return () => clearInterval(intervalId);
    }, []);

    if (isTokenExpired) {
        navigate('/')
    }
    
    useEffect(() => {
      const token = localStorage.getItem('token');
      if (!token) {
        navigate('/CustomerForm');
        return;
      }
  
      setIsLoading(false);
    }, [navigate]);
  
    if (isLoading) {
      return <div>Loading...</div>;
    }

    //--------------------------------------

    
    const rollDice = () => {
        let newDices = [];
        for (let i = 0; i < 5; i++) {
        newDices.push(Math.floor(Math.random() * 6) + 1);
        }
        setDices(newDices);
        setNbvaleur(0)
        let Numbers = dices.concat(newDices)

        if(nbessai === 5){
            try {
            axios.post('http://localhost:8080/createOneCombinaison', {
              "idCustomer" : idCustomer, "Numbers" : Numbers
            });
      
            
          } catch (error) {
            console.error(error);
          }
        }else{
            try {
                axios.put(`http://localhost:8080/updateOneCombinaison/${idCustomer}`, {
                  "Numbers" : Numbers
                });
          
                
            } catch (error) {
            console.error(error);
            }
        }

        let essai = nbessai - 1
        setNbessai(essai)
    };

    const countOccurrences = (value) => {
        return dices.reduce((acc, currentValue) => {
        return currentValue === value ? acc + 1 : acc;
        }, 0);
    };

    const statistics = {};
    for (let i = 1; i <= 6; i++) {
        statistics[i] = countOccurrences(i);
    }

    // recup valeur la plus grande
    for (let i = 0; i <= 6; i++) {
        if(nbvaleur < statistics[i]){
            setNbvaleur(statistics[i])
        }
    }


    // cadeau
    let tab = []
    if(nbessai === 5){
        return (
            <div>
                <h2>Bienvenue {Customer.Firstname} <br></br> Lancer les dès pour essayer de gagner</h2>
                <button onClick={rollDice}>Roll dice</button>
            </div>
        );
    }else{
        if(nbessai === 0 && nbvaleur < 2){
            return (
                <div>
                    <h2>Vous avez perdu</h2>
                    <p>Results: {dices.join(", ")}</p>
                    <table>
                        <thead>
                            <tr>
                                <th>/</th>
                                {Object.keys(statistics).map((value) => (
                                    <th key={value}>{value}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>nb</td>
                                {Object.keys(statistics).map((value) => (
                                    <td key={value}>{statistics[value]}</td>
                                ))}
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
        }else{
            if(nbvaleur > 1){
                tab = []
                let ChooseIdPastry
                let IdPastry = []
                for(let nb = 0 ; nb < nbvaleur ; nb++){
                    ChooseIdPastry = Math.floor(Math.random() * Pastry.length)
                    tab.push(Pastry[ChooseIdPastry].Name)
                    IdPastry.push(Pastry[ChooseIdPastry])
                }
                const Reward = () => {
                    for(let reward of IdPastry){
                        if(reward.Qty > 1){
                            try {
                            axios.put(`http://localhost:8080/updateOnePastry/${reward.id}`, {});
                            
                          } catch (error) {
                            console.error(error);
                          }
                        }else{
                            try {
                            axios.delete(`http://localhost:8080/deleteOnePastry/${reward.id}`, {});

                          
                                
                            } catch (error) {
                            console.error(error);
                            }
                        }
                    }
                    navigate('/')
                    localStorage.clear(); 
                };
                return (
                    <div>
                        <h2>Felicitation {Customer.Firstname}</h2>
                        <button onClick={Reward}>Récuperer votre récompense</button>
                        <p>Results: {dices.join(", ")}</p>
                        <table>
                            <thead>
                                <tr>
                                    <th>/</th>
                                    {Object.keys(statistics).map((value) => (
                                        <th key={value}>{value}</th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>nb</td>
                                    {Object.keys(statistics).map((value) => (
                                        <td key={value}>{statistics[value]}</td>
                                    ))}
                                </tr>
                            </tbody>
                        </table>
                        <ul>
                            {tab.map((item, index) => (
                                <li key={index}>{item}</li>
                            ))}
                        </ul>
                    </div>
                );
            }else {  return (
                    <div>
                        <h2>Dommage vous avez encore {nbessai} chance</h2>
                        <button onClick={rollDice}>Roll dice</button>
                        <p>Results: {dices.join(", ")}</p>
                        <table>
                            <thead>
                                <tr>
                                    <th>/</th>
                                    {Object.keys(statistics).map((value) => (
                                        <th key={value}>{value}</th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>nb</td>
                                    {Object.keys(statistics).map((value) => (
                                        <td key={value}>{statistics[value]}</td>
                                    ))}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                );
            }
        }
        
    }
    

    
  };

export default Game;
