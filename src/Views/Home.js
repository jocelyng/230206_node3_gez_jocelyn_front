import React, { useState, useEffect } from 'react';

const Home = () => {
  const [Pastry, setPastry] = useState([]);

  useEffect(() => {
    const fetchPastry = async () => {
      const response = await fetch('http://localhost:8080/getAllPastry');
      const Pastry = await response.json();
      setPastry(Pastry);
    };
    fetchPastry();
  }, []);

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Patisseries à Gagner</th>
          </tr>
        </thead>
        <tbody>
          {Pastry.map((pastry) => (
            <tr key={pastry.id}>
              <td>{pastry.Name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Home;