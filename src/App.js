import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Navigation from './Components/Navigation.js';
import './App.css';

function App() {
  
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <h1>Patisserie</h1>
        </header>
        <div className='main'>
          <nav>
            <ul>
              <a href="/"><li>Home</li></a>
              <a href="/CustomerForm"><li>Inscription</li></a>
              <a href="/Game"><li>Game</li></a>
            </ul>
          </nav>
          <div>
            <Navigation/>
          </div>
        </div>

      </div>
    </BrowserRouter>
  );
}

export default App;