import { Route, Routes } from 'react-router-dom';
import CustomerForm from '../Views/CustomerForm';
import Game from '../Views/Game';

import Home from '../Views/Home';

const Navigation = () => {
    
    return (
        
        <Routes>
              <Route path="/" element={<Home />}/>
              <Route path="/CustomerForm" element={<CustomerForm />}/>
              <Route path="/Game" element={<Game />}/>

        </Routes>
    )
}

export default Navigation